'use strict'; 

angular
.module( 'pacientes' )
.directive( 'datosPaciente',  function()
{
    return {
        restrict: 'A',
        controller: 'DatosPacienteController',
        controllerAs: 'vm',
        templateUrl: 'modules/pacientes/client/views/datos-paciente.client.view.html',
        scope: {
            paciente:'='
        }
    }
});
