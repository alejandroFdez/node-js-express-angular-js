'use strict'; 

angular
.module( 'pacientes' )
.directive( 'nodulosPaciente',  function()
{
    return {
        restrict: 'A',
        controller: 'NodulosPacienteController',
        controllerAs: 'vm',
        templateUrl: 'modules/pacientes/client/views/nodulos-paciente.client.view.html',
        scope: {
            paciente:'='
        }
    }
});
