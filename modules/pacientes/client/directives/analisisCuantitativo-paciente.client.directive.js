'use strict'; 

angular
.module( 'pacientes' )
.directive( 'analisisCuantitativoPaciente',  function()
{
    return {
        restrict: 'A',
        controller: 'AnalisisCuantitativoPacienteController',
        controllerAs: 'vm',
        templateUrl: 'modules/pacientes/client/views/analisisCuantitativo-paciente.client.view.html',
        scope: {
            paciente:'='
        }
    }
});
