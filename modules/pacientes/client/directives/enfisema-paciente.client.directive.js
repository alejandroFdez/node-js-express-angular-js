'use strict'; 

angular
.module( 'pacientes' )
.directive( 'enfisemaPaciente',  function()
{
    return {
        restrict: 'A',
        controller: 'EnfisemaPacienteController',
        controllerAs: 'vm',
        templateUrl: 'modules/pacientes/client/views/enfisema-paciente.client.view.html',
        scope: {
            paciente:'='
        }
    }
});
