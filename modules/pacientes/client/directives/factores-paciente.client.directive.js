'use strict'; 

angular
.module( 'pacientes' )
.directive( 'factoresPaciente',  function()
{
    return {
        restrict: 'A',
        controller: 'FactoresPacienteController',
        controllerAs: 'vm',
        templateUrl: 'modules/pacientes/client/views/factores-paciente.client.view.html',
        scope: {
            paciente:'='
        }
    }
});
