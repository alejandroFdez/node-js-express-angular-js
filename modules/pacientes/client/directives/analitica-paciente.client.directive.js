'use strict'; 

angular
.module( 'pacientes' )
.directive( 'analiticaPaciente',  function()
{
    return {
        restrict: 'A',
        controller: 'AnaliticaPacienteController',
        controllerAs: 'vm',
        templateUrl: 'modules/pacientes/client/views/analitica-paciente.client.view.html',
        scope: {
            paciente:'='
        }
    }
});
