(function () {
    'use strict'; 

    angular
    .module( 'pacientes' )
    .directive( 'pruebasFuncionalesPaciente', pruebasFuncionalesPaciente);

    function pruebasFuncionalesPaciente()
    {
        return {
            restrict: 'A',
            controller: 'PruebasFuncionalesPacienteController',
            controllerAs: 'vm',
            templateUrl: 'modules/pacientes/client/views/pruebasFuncionales-paciente.client.view.html',
            scope: {
                paciente:'='
            }
        }
    }

})();