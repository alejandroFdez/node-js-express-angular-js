(function () {
  'use strict';

  angular
    .module('pacientes.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('pacientes', {
        abstract: true,
        url: '/pacientes',
        template: '<ui-view/>'
      }) 
      .state('pacientes.list', {
        url: '',
        templateUrl: 'modules/pacientes/client/views/list-pacientes.client.view.html',
        controller: 'PacientesListController',
        controllerAs: 'vm',
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Listado'
        }
      })
      .state('pacientes.create', {
        url: '/create',
        templateUrl: 'modules/pacientes/client/views/create-paciente.client.view.html',
        controller: 'PacientesCreateController',
        controllerAs: 'vm',
        resolve: {
          pacienteResolve: newPaciente
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle : 'Nuevo'
        }
      })
      .state('pacientes.edit', {
        url: '/:pacienteId/edit',
        templateUrl: 'modules/pacientes/client/views/view-paciente.client.view.html',
        controller: 'ViewPacienteController',
        controllerAs: 'vm',
        resolve: {
          pacienteResolve: getPaciente
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Edit Paciente {{ pacienteResolve.name }}'
        }
      });
  }

  getPaciente.$inject = ['$stateParams', 'PacientesService'];

  function getPaciente($stateParams, PacientesService) {
    return PacientesService.get({
      pacienteId: $stateParams.pacienteId
    }).$promise;
  }

  newPaciente.$inject = ['PacientesService'];

  function newPaciente(PacientesService) {
    return new PacientesService();
  }
  
})();
