(function () {
  'use strict';

  angular
    .module('pacientes', ['chart.js','nvd3','localytics.directives'])
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(menuService) {
    // Set top bar menu items
    menuService.addMenuItem('topbar', {
      title: 'Pacientes',
      state: 'pacientes',
      type: 'dropdown',
      iconClass: 'fa-user',
      roles: ['user']
    });

    // Add the dropdown list item
    menuService.addSubMenuItem('topbar', 'pacientes', {
      title: 'Listado',
      state: 'pacientes.list', 
      roles: ['user']
    });

    // Add the dropdown create item
    menuService.addSubMenuItem('topbar', 'pacientes', {
      title: 'Nuevo',
      state: 'pacientes.create',
      roles: ['user']
    });
  }
})();


