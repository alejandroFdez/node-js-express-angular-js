(function () {
    'use strict';

    angular
    .module('pacientes')
    .controller('DatosPacienteController', DatosPacienteController);

    DatosPacienteController.$inject = ['$scope'];

    function DatosPacienteController( $scope ) 
    {
        var vm = this;
        vm.paciente = $scope.paciente;
        vm.error = null;
        vm.initGraficas = initGraficas;


        function initGraficas(){

            vm.lineData = {
                series: ["Nodulo 1", "Nodulo 2", "Nodulo 3"],
                labels: ["2009","2010","2011", "2012", "2013", "2014", "2015", "2016"],
                data: [
                     [6, 8, 8, 10, 14, 17,17,18],
                     [5, 6, 8, 8, 10, 11, 12, 12],
                     [6, 7, 8, 8, 9, 9, 10, 10]
                ]
            };

            vm.lineOptions = {
                legend: { display: true },
                fill: false,
                datasetFill: false,
                lineTension: 0,
                pointRadius: 0
            };
        };
    }
})();
