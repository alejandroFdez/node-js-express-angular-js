(function () {
    'use strict';

    angular
    .module('pacientes')
    .controller('PruebasFuncionalesPacienteController', PruebasFuncionalesPacienteController);

    PruebasFuncionalesPacienteController.$inject = ['$scope', 'Authentication', '$uibModal', 'DiagnosticReportsService', '$filter' ];

    function PruebasFuncionalesPacienteController( $scope, Authentication, $uibModal, DiagnosticReportsService, $filter )
    {
        var vm = this;
        vm.authentication = Authentication;
        vm.paciente = $scope.paciente;
        vm.pruebasFuncionales = DiagnosticReportsService.query({
            pacienteId: vm.paciente.resource.identifier[0].value
        }, function(){
            console.log( vm.pruebasFuncionales );
            initGraficas();
        }); 
          
        vm.chartOptions = {
            legend: { display: true }
        };
        
        
        vm.calcularIncremento = calcularIncremento;
        vm.openForm = openForm;
        vm.initGraficas = initGraficas;
        
        function calcularIncremento(indexPruebas, indexNodulos)
        {
            var incremento = ( ( vm.paciente.pruebas[indexPruebas].nodulos[indexNodulos].diametro - vm.paciente.pruebas[indexPruebas-1].nodulos[indexNodulos].diametro) / vm.paciente.pruebas[indexPruebas-1].nodulos[indexNodulos].diametro ) * 100;
            return incremento;
        }

        function initGraficas()
        {
            
            vm.FEVsData = {
                series: [],
                labels: [],
                data: [],
                colors : 
                [
                    {
                        borderColor: "#f9696c",
                        fillColor: "#ffffff",
                        fill: false
                    }
                ]
            };
            vm.FVCsData = {
                series: [],
                labels: [],
                data: [],
                colors : 
                [
                    {
                        borderColor: "#97bbcd",
                        fillColor: "#ffffff",
                        fill: false
                    }
                ]
            };
            vm.FEV1VCsData = {
                series: [],
                labels: [],
                data: [],
                colors : 
                [
                    {
                        borderColor: "#97bbcd",
                        fillColor: "#ffffff",
                        fill: false
                    }
                ]
            };
            vm.DLcosData = {
                series: [],
                labels: [],
                data: [],
                colors : 
                [
                    {
                        borderColor: "#c3c3c3",
                        fillColor: "#ffffff",
                        fill: false
                    }
                ]
            };
            
            var FEV = [];
            var FVC = [];
            var FEV1VC = [];
            var DLco = [];
            
            angular.forEach(vm.pruebasFuncionales, function( prueba, key )
            {
                FEV.push( prueba.resource.contained[0].valueQuantity.value );
                FVC.push( prueba.resource.contained[1].valueQuantity.value );
                FEV1VC.push( prueba.resource.contained[2].valueQuantity.value );
                DLco.push( prueba.resource.contained[3].valueQuantity.value );
                
                vm.FEVsData.labels.push( $filter('date')( new Date( prueba.resource.meta.lastUpdated ), "dd/MM/yyyy") );
                vm.FVCsData.labels.push( $filter('date')( new Date( prueba.resource.meta.lastUpdated ), "dd/MM/yyyy") );
                vm.FEV1VCsData.labels.push( $filter('date')( new Date( prueba.resource.meta.lastUpdated ), "dd/MM/yyyy") );
                vm.DLcosData.labels.push( $filter('date')( new Date( prueba.resource.meta.lastUpdated ), "dd/MM/yyyy") );
            });
            
            vm.FEVsData.series.push( "FEV (ml%)" );
            vm.FVCsData.series.push( "FVC (ml%)" );
            vm.FEV1VCsData.series.push( "FEV1/VC (Absolute value)" );
            vm.DLcosData.series.push( "DLco (ml%)" );
            
            vm.FEVsData.data.push( FEV );
            vm.FVCsData.data.push( FVC );
            vm.FEV1VCsData.data.push( FEV1VC );
            vm.DLcosData.data.push( DLco );
               
        };
        
        function openForm(){
            $uibModal.open({
                ariaLabelledBy: 'Prueba funcional',
                templateUrl: 'modules/pacientes/client/views/create-pruebaFuncional.client.view.html',
                size: 'lg',
                controller: 'CreatePruebaFuncionalController',
                controllerAs: 'vm',
                scope: $scope
            })
            .result.finally(function(){
                vm.pruebasFuncionales = DiagnosticReportsService.query({
                    pacienteId: vm.paciente.resource.identifier[0].value
                }, function(){
                    console.log( vm.pruebasFuncionales );
                    initGraficas();
                });
            });
        };
        
    };
    
    
    
})();
