(function () {
  'use strict';

    // Pacientes controller
    angular
      .module('pacientes')
      .controller('ViewPacienteController', ViewPacienteController);

    ViewPacienteController.$inject = [ 'Authentication', 'pacienteResolve'];

    function ViewPacienteController ( Authentication, pacienteResolve) 
    {
        var vm = this;

        vm.authentication = Authentication;
        vm.paciente = pacienteResolve;
        vm.error = null;
        vm.switchTabs = switchTabs;

        vm.tabs = {
            datos: true,
            enfisema: false,
            nodulos: false,
            factores: false,
            analisisCuantitativo: false,
            citas: false,
            analitica: false,
            pruebasFuncionales: false
        };

        function switchTabs( cual )
        {
            vm.tabs = {
                datos: false,
                enfisema: false,
                nodulos: false,
                factores: false,
                analisisCuantitativo: false,
                citas: false,
                analitica: false,
                pruebasFuncionales: false
            };
            switch( cual )
            {
                case 'D':
                    vm.tabs.datos = true;
                    break;
                case 'E':
                    vm.tabs.enfisema = true;
                    break;
                case 'N':
                    vm.tabs.nodulos = true;
                    break;
                case 'F':
                    vm.tabs.factores = true;
                    break;
                case 'AC':
                    vm.tabs.analisisCuantitativo = true;
                    break;
                case 'C':
                    vm.tabs.citas = true;
                    break;
                case 'AN':
                    vm.tabs.analitica = true;
                    break;
                case 'PF':
                    vm.tabs.pruebasFuncionales = true;
                    break;
            }
        };
    }
})();
