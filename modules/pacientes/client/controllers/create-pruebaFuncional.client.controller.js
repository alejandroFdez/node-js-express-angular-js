(function () {
  'use strict';

angular
.module('pacientes')
.controller('CreatePruebaFuncionalController', CreatePruebaFuncionalController);

CreatePruebaFuncionalController.$inject = ['$scope', '$state', 'DiagnosticReportsService', 'Authentication', '$uibModalInstance' ];

function CreatePruebaFuncionalController( $scope, $state, DiagnosticReportsService, Authentication, $uibModalInstance ) 
{
    var vm = this;
    vm.authentication = Authentication;
    vm.paciente = $scope.paciente;
    vm.pruebaFuncional = new DiagnosticReportsService;
    vm.pruebaFuncional.resource =
    {
        "resourceType": "DiagnosticReport",
        "contained": 
        [
            {
                "id": "1",
                "resourceType": "Observation",
                "valueQuantity": {
                    "unit": "ml%",
                    "value": 0
                }
            },
            {
                "id": "2",
                "resourceType": "Observation",
                "valueQuantity": {
                    "unit": "ml%",
                    "value": 0
                }
            },
            {
                "id": "3",
                "resourceType": "Observation",
                "valueQuantity": {
                    "unit": "Absolute Value",
                    "value": 0
                }
            },
            {
                "id": "4",
                "resourceType": "Observation",
                "valueQuantity": {
                    "unit": "ml%",
                    "value": 0
                }
            }
        ],
        "result": 
        [
            {
              "reference": "#1"
            },
            {
              "reference": "#2"
            },
            {
              "reference": "#3"
            },
            {
              "reference": "#4"
            }
        ]
    };
    
    vm.error = null;
    vm.form = {};
    vm.save = save;
    vm.cancel = cancel;
            
    function cancel() {
        $uibModalInstance.dismiss('cancel');
    };
    // Create new Paciente
    
    // Save Paciente
    function save(isValid) {
        if (!isValid) {
            $scope.$broadcast('show-errors-check-validity', 'vm.form.pruebasFuncionales');
            return false;
        }

        vm.pruebaFuncional.pacienteId = vm.paciente.resource.identifier[0].value;

        // TODO: move create/update logic to service
        if ( vm.pruebaFuncional.resource.id ) {
            vm.pruebaFuncional.$update(successCallback, errorCallback);
        } else {
            vm.pruebaFuncional.resource.subject = 
            {
                "reference":"Patient/" + vm.paciente.resource.id,
                "display": vm.paciente.resource.name[0].given[0] + " " + vm.paciente.resource.name[0].family[0]
            };
            vm.pruebaFuncional.$save(successCallback, errorCallback);
            
        }

        function successCallback(res) {
            console.log( vm.pruebaFuncional );
            cancel();
        }

        function errorCallback(res) {
            vm.error = res.data.message;
        }
    };
    
}
})();

