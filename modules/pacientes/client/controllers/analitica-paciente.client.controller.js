(function () {
    'use strict';

    angular
    .module('pacientes')
    .controller('AnaliticaPacienteController', AnaliticaPacienteController);

    AnaliticaPacienteController.$inject = ['$scope', 'Authentication', '$state'];

    function AnaliticaPacienteController( $scope, Authentication, $state )
    {
        var vm = this;
        vm.authentication = Authentication;
        vm.paciente = $scope.paciente;
        vm.initGraficas = initGraficas();

        function initGraficas()
        {
            vm.options = {
                chart: {
                    type: 'bulletChart',
                    transitionDuration: 100
                }
            };

            vm.data = {
                "title": "",
                "ranges": [3.90,7,10.20],
                "measures": [5.83],
                "markers": [5.83]
            };
        }
        
    };
    
})();
