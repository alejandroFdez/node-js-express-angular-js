(function () {
    'use strict';

    angular
    .module('pacientes')
    .controller('FactoresPacienteController', FactoresPacienteController);

    FactoresPacienteController.$inject = ['$scope', 'QuestionnairesService', 'FamilyMemberHistoriesService', 'Authentication'];

    function FactoresPacienteController( $scope, QuestionnairesService, FamilyMemberHistoriesService, Authentication ) 
    {
        var vm = this;
        vm.authentication = Authentication;
        
        vm.error = null;
        vm.form = {};
        vm.save = save;
        vm.paciente = $scope.paciente;
        vm.familiares = ["Padre", "Madre", "Abuelo", "Abuela"];
        vm.enfermedades = [];
        vm.cambioFactor = cambioFactor;
        vm.newFamilyMemberHistory = newFamilyMemberHistory;
        
        vm.questionnaireResponse = QuestionnairesService.get({
            pacienteId: vm.paciente.resource.identifier[0].value
        }, function(){
            console.log( vm.questionnaireResponse );
            angular.forEach( vm.questionnaireResponse.resource.group.question[10].answer , function( enfermedad, key){
                vm.enfermedades.push( enfermedad.valueString );
            });
            console.log( vm.enfermedades )
            $('#trastornosEnfermedades').val(vm.enfermedades).trigger('chosen:updated');
        });
        
        vm.familyMemberHistories = FamilyMemberHistoriesService.query({
            pacienteId: vm.paciente.resource.identifier[0].value
        }, function(){
            console.log( vm.familyMemberHistories );
        }); 
        
        function newFamilyMemberHistory()
        {
            var familyMemeberHistory = new FamilyMemberHistoriesService;
            familyMemeberHistory.resource = 
            {
                "name": "",
                "condition":
                [
                    {
                        "code":                     
                        {
                            "note": ""
                        }        
                    }
                ]
                
            };
            
            return familyMemeberHistory;
        };
        
        
       
        function cambioFactor(tipo, valor)
        {
            switch(tipo)
            {
                case 'EC':
                    if( valor )
                    {
                        vm.questionnaireResponse.resource.group.question[1].answer = [''];
                    }
                    else
                    {
                        vm.questionnaireResponse.resource.group.question[1].answer = [];
                    }
                break;
                case 'AF':
                    if( valor )
                    {
                        vm.familyMemberHistories =[];
                        vm.familyMemberHistories.push( newFamilyMemberHistory() );
                    }
                    else
                    {
                        vm.familyMemberHistories = [];
                    }
                break;
                case 'AP':
                    if( valor )
                    {
                        vm.questionnaireResponse.resource.group.question[4].answer = [''];
                    }
                    else
                    {
                        vm.questionnaireResponse.resource.group.question[4].answer = [];
                    }
                break;
                case 'FA':
                    if( valor )
                    {
                        vm.questionnaireResponse.resource.group.question[6].answer.valueBoolean = false;
                        vm.questionnaireResponse.resource.group.question[7].answer.valueInteger;
                    }
                break;
                case 'EF':
                    if( valor )
                    {
                        vm.questionnaireResponse.resource.group.question[5].answer.valueBoolean;
                    }
                break;
            }
        };
        
        function save(isValid) {
            if (!isValid) {
                $scope.$broadcast('show-errors-check-validity', 'vm.form.pacienteFormFactores');
                return false;
            }
            
            vm.questionnaireResponse.pacienteId = vm.paciente.resource.identifier[0].value;
            vm.questionnaireResponse.resource.group.question[10].answer = [];
            
            angular.forEach( vm.enfermedades, function( enfermedad, key){
                vm.questionnaireResponse.resource.group.question[10].answer.push( { "valueString" : enfermedad});
            });
            
            if ( vm.questionnaireResponse.resource.id ) 
            {
                vm.questionnaireResponse.id = vm.questionnaireResponse.resource.id;
                vm.questionnaireResponse.$update(successCallback, errorCallback);
            } 
            else 
            {
                vm.questionnaireResponse.resource.subject = 
                {
                    "reference":"Patient/" + vm.paciente.resource.id,
                    "display": vm.paciente.resource.name[0].given[0] + " " + vm.paciente.resource.name[0].family[0]
                };
                vm.questionnaireResponse.$save(successCallback, errorCallback);
            }
            
            angular.forEach( vm.familyMemberHistories, function(familyMemberHistory, key){
                familyMemberHistory.resource.patient = 
                {
                    "reference":"Patient/" + vm.paciente.resource.id,
                    "display": vm.paciente.resource.name[0].given[0] + " " + vm.paciente.resource.name[0].family[0]
                };
                familyMemberHistory.pacienteId = vm.paciente.resource.identifier[0].value;
                familyMemberHistory.$save( successCallbackFamily, errorCallbackFamily );
            });
            
            
            function successCallback() {
                console.log( vm.questionnaireResponse )
            }

            function errorCallback(res) {
                vm.error = res.message;
            }
            
            function successCallbackFamily( res ) {
                console.log( res )
            }

            function errorCallbackFamily(res) {
                vm.error = res.message;
            }
        };
        
    };
    
})();