(function () {
    'use strict';

    angular
    .module('pacientes')
    .controller('AnalisisCuantitativoPacienteController', AnalisisCuantitativoPacienteController);

    AnalisisCuantitativoPacienteController.$inject = ['$scope', 'Authentication', '$state'];

    function AnalisisCuantitativoPacienteController( $scope, Authentication, $state ) 
    {
        var vm = this;
        vm.authentication = Authentication;
        vm.paciente = $scope.paciente;
        
    };
    
})();