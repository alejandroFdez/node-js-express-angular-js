(function () {
    'use strict';

    angular
    .module('pacientes')
    .controller('NodulosPacienteController', NodulosPacienteController);

    NodulosPacienteController.$inject = ['$scope'];

    function NodulosPacienteController( $scope ) 
    {
        var vm = this;
        vm.paciente = $scope.paciente;
        
        vm.paciente.pruebas = 
        [
            {
                "id": 1,
                "fecha": "12/01/2016", 
                "nodulos": [
                    {
                    "id": 1,
                    "tipo": "Sólido",
                    "localizacion": "IM",
                    "diametro": 3
                    },
                    {
                    "id": 2,
                    "tipo": "Mixto",
                    "localizacion": "DB",
                    "diametro": 4
                    },
                    {
                    "id": 3,
                    "tipo": "VD",
                    "localizacion": "DM",
                    "diametro": 6
                    }                    
                ]
            },
            {
                "id": 2,
                "fecha": "23/03/2016",
                "nodulos": [
                    {
                    "id": 1,
                    "tipo": "Sólido",
                    "localizacion": "IM",
                    "diametro": 5
                    },
                    {
                    "id": 2,
                    "tipo": "Mixto",
                    "localizacion": "DB",
                    "diametro": 4
                    },
                    {
                    "id": 3,
                    "tipo": "VD",
                    "localizacion": "DM",
                    "diametro": 7
                    }                    
                ]
            },
            {
                "id": 3,
                "fecha": "19/05/2016",
                "nodulos": [
                    {
                    "id": 1,
                    "tipo": "Sólido",
                    "localizacion": "IM",
                    "diametro": 7
                    },
                    {
                    "id": 2,
                    "tipo": "Mixto",
                    "localizacion": "DB",
                    "diametro": 5
                    },
                    {
                    "id": 3,
                    "tipo": "VD",
                    "localizacion": "DM",
                    "diametro": 7
                    }                    
                ]
            },
            {
                "id": 4,
                "fecha": "06/07/2016",
                "nodulos": [
                    {
                    "id": 1,
                    "tipo": "Sólido",
                    "localizacion": "IM",
                    "diametro": 7
                    },
                    {
                    "id": 2,
                    "tipo": "Mixto",
                    "localizacion": "DB",
                    "diametro": 6
                    },
                    {
                    "id": 3,
                    "tipo": "VD",
                    "localizacion": "DM",
                    "diametro": 7
                    }                    
                ]
            },
            {
                "id": 5,
                "fecha": "27/09/2016",
                "nodulos": [
                    {
                    "id": 1,
                    "tipo": "Sólido",
                    "localizacion": "IM",
                    "diametro": 8
                    },
                    {
                    "id": 2,
                    "tipo": "Mixto",
                    "localizacion": "DB",
                    "diametro": 6
                    },
                    {
                    "id": 3,
                    "tipo": "VD",
                    "localizacion": "DM",
                    "diametro": 8
                    }                    
                ]
            },
            {
                "id": 6,
                "fecha": "08/12/2016",
                "nodulos": [
                    {
                    "id": 1,
                    "tipo": "Sólido",
                    "localizacion": "IM",
                    "diametro": 9
                    },
                    {
                    "id": 2,
                    "tipo": "Mixto",
                    "localizacion": "DB",
                    "diametro": 7
                    },
                    {
                    "id": 3,
                    "tipo": "VD",
                    "localizacion": "DM",
                    "diametro": 8
                    }                    
                ]
            },
            {
                "id": 7,
                "fecha": "04/01/2017",
                "nodulos": [
                    {
                    "id": 1,
                    "tipo": "Sólido",
                    "localizacion": "IM",
                    "diametro": 9
                    },
                    {
                    "id": 2,
                    "tipo": "Mixto",
                    "localizacion": "DB",
                    "diametro": 7
                    },
                    {
                    "id": 3,
                    "tipo": "VD",
                    "localizacion": "DM",
                    "diametro": 10
                    }                    
                ]
            },
            {
                "id": 8,
                "fecha": "06/03/2017",
                "nodulos": [
                    {
                    "id": 1,
                    "tipo": "Sólido",
                    "localizacion": "IM",
                    "diametro": 11
                    },
                    {
                    "id": 2,
                    "tipo": "Mixto",
                    "localizacion": "DB",
                    "diametro": 7
                    },
                    {
                    "id": 3,
                    "tipo": "VD",
                    "localizacion": "DM",
                    "diametro": 10
                    }                    
                ]
            }
        ];
        
        vm.initGraficas = initGraficas;
        vm.calcularIncremento = calcularIncremento;
        
        function calcularIncremento(indexPruebas, indexNodulos)
        {
            var incremento = ( ( vm.paciente.pruebas[indexPruebas].nodulos[indexNodulos].diametro - vm.paciente.pruebas[indexPruebas-1].nodulos[indexNodulos].diametro) / vm.paciente.pruebas[indexPruebas-1].nodulos[indexNodulos].diametro ) * 100;
            return incremento;
        }

        function initGraficas()
        {
            
            vm.nodulesData = {
                series: [],
                labels: [],
                data: []
            };
                        
            angular.forEach(vm.paciente.pruebas[0].nodulos, function(noduloKey, key)
            {
                var diametros = [];
                
                angular.forEach(vm.paciente.pruebas, function(prueba){
                    diametros.push( prueba.nodulos[key].diametro ); 
                    
                    if(key == 0){
                        vm.nodulesData.labels.push( prueba.fecha );
                    }
                });
                
                vm.nodulesData.data.push( diametros );
                vm.nodulesData.series.push( "Nodulo " + noduloKey.id );
            });

            vm.nodulesOptions = {
                legend: { display: true }
            };
            
            vm.nodulesColors = [
                {
                    borderColor: "#97bbcd",
                    fillColor: "#ffffff",
                    fill: false
                },
                {
                    borderColor: "#f9696c",
                    fillColor: "#ffffff",
                    fill: false
                },
                {
                    borderColor: "#c3c3c3",
                    fillColor: "#ffffff",
                    fill: false
                }
            ];        
        };
    };
})();