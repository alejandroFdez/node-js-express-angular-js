(function () {
  'use strict';

angular
.module('pacientes')
.controller('PacientesCreateController', PacientesCreateController);

PacientesCreateController.$inject = ['$scope', '$state', 'pacienteResolve', 'Authentication' ];

function PacientesCreateController( $scope, $state, pacienteResolve, Authentication ) 
{
    var vm = this;
    vm.authentication = Authentication;
    vm.paciente = pacienteResolve;
    vm.error = null;
    vm.form = {};
    vm.save = save;
    // Create new Paciente
    
    // Save Paciente
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.pacienteForm');
        return false;
      }

      // TODO: move create/update logic to service
      if (vm.paciente._id) {
        vm.paciente.$update(successCallback, errorCallback);
      } else {
        vm.paciente.$save(successCallback, errorCallback);
      }

      function successCallback(res) {
        $state.go('pacientes.edit', {
          pacienteId: res._id
        });
      }

      function errorCallback(res) {
        vm.error = res.data.message;
      }
    };
    
}
})();

