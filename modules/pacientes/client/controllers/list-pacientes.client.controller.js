(function () {
  'use strict';

  angular
    .module('pacientes')
    .controller('PacientesListController', PacientesListController);

  PacientesListController.$inject = ['PacientesService'];

  function PacientesListController(PacientesService) {
    var vm = this;

    vm.pacientes = PacientesService.query(function(data) {
        vm.seleccionarPaciente(data[0]);
    });
    
    console.log( "PACIENTES" );
    console.log( vm.pacientes );

    vm.seleccionarPaciente = function( paciente ) {
            vm.pacienteSeleccionado = paciente;
    };
  }
})();
