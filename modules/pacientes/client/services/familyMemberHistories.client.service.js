// FamilyMemberHistory service used to communicate FamilyMemberHistories REST endpoints
(function () {
    'use strict';

    angular
    .module('pacientes.services')
    .factory('FamilyMemberHistoriesService', FamilyMemberHistoriesService);

    FamilyMemberHistoriesService.$inject = ['$resource'];

    function FamilyMemberHistoriesService($resource) { 
        return $resource
        (
            'api/pacientes/:pacienteId/familyMemberHistories/:familyMemberHistoryId', 
            {
                pacienteId: '@pacienteId',
                questionnaireId: '@id'
            }, 
            {
                update: {
                    method: 'PUT'
                }
            }
        );
    };
  
}());

