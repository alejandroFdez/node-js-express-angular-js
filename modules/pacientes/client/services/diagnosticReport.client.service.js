(function () {
  'use strict';

  angular
    .module('pacientes.services')
    .factory('DiagnosticReportsService', DiagnosticReportsService);

  DiagnosticReportsService.$inject = ['$resource'];

  function DiagnosticReportsService($resource) {
    return $resource
    (
        'api/pacientes/:pacienteId/diagnosticReports/:diagnosticReportId', 
        {
            pacienteId: '@pacienteId',
            diagnosticReportId: '@id'
        },
        {
            update: { method:'PUT' }
        }
    );
  }
})();
