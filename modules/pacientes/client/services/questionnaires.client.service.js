(function () {
  'use strict';

  angular
    .module('pacientes.services')
    .factory('QuestionnairesService', QuestionnairesService);

  QuestionnairesService.$inject = ['$resource'];

  function QuestionnairesService($resource) {
    return $resource
    (
        'api/pacientes/:pacienteId/questionnaires/:questionnaireId', 
        {
            pacienteId: '@pacienteId',
            questionnaireId: '@id'
        },
        {
            update: { method:'PUT' }
        }
    );
  }
})();
