'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mkFhir = require('fhir.js'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');


var client = mkFhir({
    //baseUrl: '####'
});

// baseUrl: 'http://fhir3.healthintersections.com.au/open/Patient/:pacienteId?_format=application/json'
/**
 * Create a Paciente
 */
exports.create = function(req, res) {
    
    delete req.body.pacienteId;
    req.body.resource.resourceType = "FamilyMemberHistory";
    
    
    client
    .create(req.body)
    .then(function(resFhir){
        
        var bundle =
        {
            "resource": resFhir.data
        };

        res.jsonp( bundle );
        
    })
    .catch(function(resFhir){
        //Error responses
        if (resFhir.status){
            console.log('Error', resFhir.status);
        }

        //Errors
        if (resFhir.message){
            console.log('resFhir', resFhir.message);
        }
        res.jsonp( resFhir );
    });
};

/**
 * Show the current Paciente
 */
exports.read = function(req, res) {
  
  client
    .search( { type: 'FamilyMemberHistory', query: { patient : req.paciente.resource.id } } )
    .then(function(resFhir){
        
        var bundle = resFhir.data;
        if(!bundle.entry[0])
        {
            res.status(404);
            res.jsonp( bundle );
        }
        else
        {
            res.jsonp( bundle.entry[0] );
        }
        
    })
    .catch(function(resFhir){
        //Error responses
        if (resFhir.status){
            console.log('Error', resFhir.status);
        }

        //Errors
        if (resFhir.message){
            console.log('Error', res.message);
        }
    });
  
};

/**
 * Update a Paciente
 */
exports.update = function(req, res) {
  var paciente = req.paciente ;

  paciente = _.extend(paciente , req.body);

  paciente.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(paciente);
    }
  });
};

/**
 * Delete an FamilyMemberHistory
 */
exports.delete = function(req, res) {
  var paciente = req.paciente ;

  paciente.remove(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(paciente);
    }
  });
};

/**
 * List of FamilyMemberHistories
 */
exports.list = function(req, res) { 
    
    client
    .search( { type: 'FamilyMemberHistory', query: { patient : req.paciente.resource.id } })
    .then(function(resFhir){
        
        var bundle = resFhir.data;
        if(resFhir.data.total == 0)
        {
            res.jsonp( [] );
        }
        else
        {
            res.jsonp( bundle.entry );
        }
        console.log(bundle)
    })
    .catch(function(resFhir){
        //Error responses
        if (resFhir.status){
            console.log('Error', resFhir.status);
        }

        //Errors
        if (resFhir.message){
            console.log('Error', res.message);
        }
    });

};


/**
 * familyMemberHistory middleware
 */
exports.familyMemberHistoryByID = function(req, res, next, id) {

    client
    .search( {type: 'FamilyMemberHistory', query: {"id" : id }  })
    .then(function(resFhir){
        console.log("RESPONSE OK")
        req.questionnaire = resFhir.data.entry[0]; 
        console.log("QUESTIONNAIRE")
        console.log(req.questionnaire)
        next();
    })
    .catch(function(resFhir){
        //Error responses
        console.log("RESPONSE ERROR")
        console.log(resFhir)
        return next(resFhir);
        
    });  
};
