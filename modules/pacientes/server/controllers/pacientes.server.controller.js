'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mkFhir = require('fhir.js'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');


var client = mkFhir({
    //baseUrl: '####'
});

/**
 * Create a Paciente
 */
exports.create = function(req, res) {
  var paciente = new Paciente(req.body);
  paciente.user = req.user;

  /*paciente.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(paciente);
    }
  });*/
};

/**
 * Show the current Paciente
 */
exports.read = function(req, res) {
  // convert mongoose document to JSON
  console.log("dflhsdojfgewvfuge")
  console.log(req.paciente)
  //var paciente = req.paciente ? req.paciente.toJSON() : {};
  var paciente = req.paciente;

  // Add a custom field to the Article, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  //paciente.isCurrentUserOwner = req.user && paciente.user && paciente.user._id.toString() === req.user._id.toString() ? true : false;

  res.jsonp(paciente);
};

/**
 * Update a Paciente
 */
exports.update = function(req, res) {
  var paciente = req.paciente ;

  paciente = _.extend(paciente , req.body);

  paciente.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(paciente);
    }
  });
};

/**
 * Delete an Paciente
 */
exports.delete = function(req, res) {
  var paciente = req.paciente ;

  paciente.remove(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(paciente);
    }
  });
};

/**
 * List of Pacientes
 */
exports.list = function(req, res) { 
    client
    .search( {type: 'Patient'})
    .then(function(resFhir){
        
        var bundle = resFhir.data;
        var count = ( bundle.entry && bundle.entry.length ) || 0;
        console.log(bundle.entry[0]);
        res.jsonp( bundle.entry );
        console.log("# Patients born in 1974: ", count);
        console.log(bundle)
    })
    .catch(function(resFhir){
        //Error responses
        if (resFhir.status){
            console.log('Error', resFhir.status);
        }

        //Errors
        if (resFhir.message){
            console.log('Error', res.message);
        }
    });

    /*Paciente
    .find()
    .sort('-created')
    .populate('user', 'displayName')
    .exec(function(err, pacientes) 
    {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(pacientes);
        }
    });*/
};


/**
 * Paciente middleware
 */
exports.pacienteByID = function(req, res, next, id) {

  /*if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Paciente is invalid'
    });
  }

  Paciente.findById(id).populate('user', 'displayName').exec(function (err, paciente) {
    if (err) {
      return next(err);
    } else if (!paciente) {
      return res.status(404).send({
        message: 'No Paciente with that identifier has been found'
      });
    }
    req.paciente = paciente;
    next();
  });*/
    client
    .search( {type: 'Patient', query: {"identifier" : id }  })
    .then(function(resFhir){
        req.paciente = resFhir.data.entry[0]; 
        console.log("PACIENTE")
        console.log(req.paciente) 
        next();
    })
    .catch(function(resFhir){
        //Error responses
        if (resFhir.status){
            console.log('Error', resFhir.status);
        }

        //Errors
        if (resFhir.message){
            console.log('resFhir', resFhir.message);
        }
        res.jsonp( resFhir );
    });  
};
