'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mkFhir = require('fhir.js'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  QuestionnaireResponse_FactoresRiesgo = require("../models/QuestionnaireResponse_FactoresRiesgo.json"),
  _ = require('lodash');


var client = mkFhir({
    //baseUrl: '####'
});

// baseUrl: 'http://fhir3.healthintersections.com.au/open/Patient/:pacienteId?_format=application/json'
/**
 * Create a QuestionnaireResponse
 */
exports.create = function(req, res) {
    
    delete req.body.pacienteId;
    
    client
    .create(req.body)
    .then(function(resFhir){
        
        var bundle = {
            resource: resFhir.data
        };

        res.jsonp( bundle );
        
    })
    .catch(function(resFhir){
        //Error responses
        if (resFhir.status){
            console.log('Error', resFhir.status);
        }

        //Errors
        if (resFhir.message){
            console.log('resFhir', resFhir.message);
        }
        res.jsonp( resFhir );
    });
};

/**
 * Show the current QuestionnaireResponse
 */
exports.read = function(req, res) {
  
  client
    .search( { type: 'QuestionnaireResponse', query: { patient : req.paciente.resource.id } } )
    .then(function(resFhir){
        
        var bundle = resFhir.data;
        if( bundle.total == 0 )
        {
            res.jsonp( QuestionnaireResponse_FactoresRiesgo );
        }
        else
        {
            res.jsonp( bundle.entry[0] );
        }
        
    })
    .catch(function(resFhir){
        //Error responses
        if (resFhir.status){
            console.log('Error', resFhir.status);
        }

        //Errors
        if (resFhir.message){
            console.log('Error', res.message);
        }
    });
  
};

/**
 * Update a QuestionnaireResponse
 */
exports.update = function(req, res) {
    
    delete req.body.pacienteId;
    delete req.body.id;
    
    client
    .update(req.body)
    .then(function(resFhir){
        
        var bundle = {
            resource: resFhir.data
        };
        
        res.jsonp( bundle );
        
    })
    .catch(function(resFhir){
        //Error responses
        if (resFhir.status){
            console.log('Error', resFhir.status);
        }

        //Errors
        if (resFhir.message){
            console.log('resFhir', resFhir.message);
        }
        res.jsonp( resFhir );
    });
    
    
  /*var paciente = req.paciente ;

  paciente = _.extend(paciente , req.body);

  paciente.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(paciente);
    }
  });*/
};

/**
 * Delete an QuestionnaireResponse
 */
exports.delete = function(req, res) {
  var paciente = req.paciente ;

  paciente.remove(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(paciente);
    }
  });
};

/**
 * List of Pacientes
 */
exports.list = function(req, res) { 
    client
    .search( {type: 'Patient'})
    .then(function(resFhir){
        
        var bundle = resFhir.data;
        var count = ( bundle.entry && bundle.entry.length ) || 0;
        console.log(bundle.entry[0]);
        res.jsonp( bundle.entry );
        console.log("# Patients born in 1974: ", count);
        console.log(bundle)
    })
    .catch(function(resFhir){
        //Error responses
        if (resFhir.status){
            console.log('Error', resFhir.status);
        }

        //Errors
        if (resFhir.message){
            console.log('Error', res.message);
        }
    });

};


/**
 * QuestionnaireResponse middleware
 */
exports.questionnaireByID = function(req, res, next, id) {

    client
    .search( {type: 'QuestionnaireResponse', query: { "_id" : id }  })
    .then(function(resFhir){
        console.log("RESPONSE OK")
        req.questionnaireResponse = resFhir.data.entry[0]; 
        console.log("QUESTIONNAIRE RESPONSE")
        console.log(req.questionnaireResponse)
        next();
    })
    .catch(function(resFhir){
        //Error responses
        if (resFhir.status){
            console.log('Error', resFhir.status);
        }

        //Errors
        if (resFhir.message){
            console.log('resFhir', resFhir.message);
        }
        console.log(resFhir.data.issue[0].diagnostics)
        res.jsonp( resFhir );
        
    });  
};
