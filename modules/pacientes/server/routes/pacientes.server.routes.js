'use strict';

/**
 * Module dependencies
 */
var pacientesPolicy = require('../policies/pacientes.server.policy'),
  pacientes = require('../controllers/pacientes.server.controller'),
  citas = require('../../../citas/server/controllers/citas.server.controller'),
  questionnaires = require('../controllers/questionnaires.server.controller'),
  diagnosticReports = require('../controllers/diagnosticReports.server.controller'),
  familyMemberHistories = require('../controllers/familyMemberHistories.server.controller');

module.exports = function(app) {
  // Pacientes Routes
  app.route('/api/pacientes').all(pacientesPolicy.isAllowed)
    .get(pacientes.list)
    .post(pacientes.create);

  app.route('/api/pacientes/:pacienteId').all(pacientesPolicy.isAllowed)
    .get(pacientes.read)
    .put(pacientes.update)
    .delete(pacientes.delete);
    
  app.route('/api/pacientes/:pacienteId/citas').all(pacientesPolicy.isAllowed)
    .get(citas.list);
    
  app.route('/api/pacientes/:pacienteId/questionnaires').all(pacientesPolicy.isAllowed)
    .get(questionnaires.read)
    .post(questionnaires.create);
    
  app.route('/api/pacientes/:pacienteId/familyMemberHistories').all(pacientesPolicy.isAllowed)
    .get(familyMemberHistories.list)
    .post(familyMemberHistories.create);
    
  app.route('/api/pacientes/:pacienteId/diagnosticReports').all(pacientesPolicy.isAllowed)
    .get(diagnosticReports.list)
    .post(diagnosticReports.create);
    
  app.route('/api/pacientes/:pacienteId/questionnaires/:questionnaireId').all(pacientesPolicy.isAllowed)
    .get(questionnaires.read)
    .put(questionnaires.update); 
  
  app.route('/api/pacientes/:pacienteId/familyMemberHistories/:familyMemberHistoryId').all(pacientesPolicy.isAllowed)
    .get(familyMemberHistories.read)
    .put(familyMemberHistories.update); 

  // Finish by binding the Paciente middleware
  app.param('pacienteId', pacientes.pacienteByID);
  
  // Finish by binding the Paciente middleware
  app.param('questionnaireId', questionnaires.questionnaireByID);
  
  // Finish by binding the Paciente middleware
  app.param('familyMemberHistoryId', familyMemberHistories.familyMemberHistoryByID);
};
