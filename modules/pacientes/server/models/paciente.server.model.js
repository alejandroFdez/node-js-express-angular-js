//'use strict';
//
///**
// * Module dependencies.
// */
//var mongoose = require('mongoose'),
//  Schema = mongoose.Schema;
//
///**
// * Paciente Schema
// */
//var PacienteSchema = new Schema({
//  nombre: {
//    type: String,
//    default: '',
//    required: 'Por favor, rellene el nombre del paciente'
//  },
//  apellidos: {
//    type: String,
//    default: '',
//    required: 'Por favor, rellene los apellidos del paciente'
//  },
//  fechaNacimiento: {
//    type: Date,
//    default: '',
//    required: 'Por favor, rellene la fecha de nacimiento',
//  },
//  sexo: {
//    type: String,
//    default: '',
//    required: 'Por favor, rellene el sexo del paciente'
//  },
//  peso: {
//    type: String,
//    default: '',
//    required: 'Por favor, rellene peso del paciente'
//  },
//  altura: {
//    type: String,
//    default: '',
//    required: 'Por favor, rellene la altura del paciente'
//  },
//  fumador: {
//    type: Boolean,
//    required: 'Por favor, rellene si es fumador'
//  },
//  numeroPaquetes: {
//    type: Number
//  },
//  colesterol: {
//    type: String,
//    default: '',
//    required: 'Por favor, rellene los apellidos del paciente'
//  },
//  hemoglobina: {
//    type: String,
//    default: '',
//    required: 'Por favor, rellene los apellidos del paciente'
//  },
//  tiempoSiguienteCita: {
//    type:Number
//  },
//  enfisema: {
//    aparicion: {
//      type: String,
//      trim: true,
//      default: 'nuevo'
//    },
//    testsEnfisema: [{
//      fecha: {
//        type: Date
//      },
//      bullaIndex: {
//        type: Number,
//        default: 0
//      },
//      localizacion: {
//        type: String,
//        default: ''
//      }
//    }]
//  },
//  factoresDeRiesgo: {
//    exposicionACancerigenos: {
//      type: Boolean
//    },
//    materialesCancerigenos: [{
//      type:String
//    }],
//    tieneAntecedentesFamiliares: {
//      type: Boolean
//    },
//    antecedentesFamiliares: [{
//      familiar:{
//          type: String
//      },
//      cancer:{
//          type: String
//      }
//    }],
//    tieneAntecedentesPersonales: {
//      type: Boolean
//    },
//    antecedentesPersonales: [{
//      type:String
//    }],
//    enfermedadesPulmonares: [{
//      type: String
//    }],
//    medicamentosActuales: [{
//      type: String
//    }]
//  },
//  nodulos: [{
//    tipoNodulo: {
//      type: String,
//      default: ''
//    },
//    aparicion: {
//      type: String,
//      trim: true,
//      default: 'nuevo'
//    },
//    ultimoTest: {
//      type: Date
//    },
//    diametro: {
//      type: Number
//    },
//    lolizacion: {
//        type: String,
//        default: ''
//    },
//    testsNodule: [{
//      fecha: {
//        type: Date
//      },
//      diametro: {
//        type: Number
//      },
//      localizacion: {
//        type: String,
//        default: ''
//      }
//    }]
//  }],
//  created: {
//    type: Date,
//    default: Date.now
//  },
//  user: {
//    type: Schema.ObjectId,
//    ref: 'User'
//  }
//});
//
//mongoose.model('Paciente', PacienteSchema);
