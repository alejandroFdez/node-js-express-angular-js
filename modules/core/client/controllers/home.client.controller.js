(function () {
    'use strict';

    angular
    .module('core')
    .controller('HomeController', HomeController);

    function HomeController() 
    {
        var vm = this;
        vm.initGraficas = initGraficas;

        var date = new Date();
        date.setMonth(date.getMonth() - 1);

        vm.year = date.getFullYear()-1;
        vm.lastYear = date.getFullYear()-2;

        var objDate = new Date(date),
        locale = "es-es",
        month = objDate.toLocaleString(locale, { month: "long" });
        vm.month = month.charAt(0).toUpperCase() + month.slice(1);
        
        function initGraficas(){


            vm.optionsScatterPlot = {
            chart: {
                type: 'scatterChart',
                height: 450,
                color: d3.scale.category10().range(),
                scatter: {
                    onlyCircles: false
                },
                showDistX: true,
                showDistY: true,
                tooltipContent: function(key) {
                    return '<h3>' + key + '</h3>';
                },
                duration: 350,
                xAxis: {
                    axisLabel: 'X Axis',
                    tickFormat: function(d){
                        return d3.format('')(d);
                    },
                    "axisLabel": " Tamaño Nódulo (mmm) ",
                },
                yAxis: {
                    axisLabel: 'Y Axis',
                    tickFormat: function(d){
                        return d3.format('')(d);
                    },
                    "axisLabel": "Edad",
                    axisLabelDistance: -5
                },
                zoom: {
                    //NOTE: All attributes below are optional
                    enabled: false,
                    scaleExtent: [1, 10],
                    useFixedDomain: false,
                    useNiceScale: false,
                    horizontalOff: false,
                    verticalOff: false,
                    unzoomEventType: 'dblclick.zoom'
                },
                yDomain: [40,80],
                xDomain: [1,20]
            }
        };

        vm.dataScatterPlot = generateData(1,140);

        /* Random Data Generator (took from nvd3.org) */
        function generateData(groups, points) {
            var data = [],
                shapes = ['circle'];

            for (var i = 0; i < groups; i++) {
                data.push({
                    key: 'Group ' + i,
                    values: []
                });

                for (var j = 0; j < points; j++) {
                    data[i].values.push({
                        x: Math.floor(Math.random() * (20 - 1 + 1)) + 1
                        , y: Math.floor(Math.random() * (65 - 45 + 1)) + 45
                        , size: Math.random()
                        , shape: shapes[j % 6]
                    });
                }
            }
            return data;
        }

            vm.xminvalue = -10;
            vm.xmaxvalue = 3000;
            vm.options = {
                chart: {
                    type: 'multiChart',
                    height: 350,
                    margin : {
                        top: 60,
                        right: 60,
                        bottom: 50,
                        left: 70
                    },
                color: d3.scale.category10().range(),
                //useInteractiveGuideline: true,
                //duration: 500,
                xAxis: {
                    tickFormat: function(d){
                        return d3.format('')(d);
                    },
                    "axisLabel": " Años",

                },
                yAxis1: {
                    tickFormat: function(d){
                        return d3.format('')(d);
                    },
                    
                    "axisLabel": "Media de edad",
                },
                yAxis2: {
                    tickFormat: function(d){
                        return d3.format('')(d);
                    },
                    "axisLabel": "Nº Pacientes",
                },
                yDomain1: [0,100]

                
            }
        };

        

        refresChart();

        function refresChart(){
            vm.options.chart.xDomain = [vm.xminvalue,vm.xmaxvalue];
        }


        vm.options.chart.forceY = [0, 10 * 1.2];

        //vm.options.chart.yAxis1.forceY([0,100]);

        vm.data = [
        {
            "key":"Media de edad",
            "values":[
            {
                "x":2009,
                "y": 58
            },
            {
                "x":2010,
                "y": 63
            },
            {
                "x":2011,
                "y": 61
            },
            {
                "x":2012,
                "y": 54
            },
            {
                "x":2013,
                "y": 50
            },
            {
                "x":2014,
                "y": 49
            },
            {
                "x":2015,
                "y": 47
            },
            {
                "x":2016,
                "y": 43
            }
            ],
            "type":"line",
            "yAxis":1
        },
        {
            "key":"Hombres",
            "values":[
            {
                "x":2009,
                "y": 70
            },
            {
                "x":2010,
                "y": 80
            },
            {
                "x":2011,
                "y": 65
            },
            {
                "x":2012,
                "y": 54
            },
            {
                "x":2013,
                "y": 93
            },
            {
                "x":2014,
                "y": 71
            },
            {
                "x":2015,
                "y": 25
            },
            {
                "x":2016,
                "y": 56
            }
            ],
            "type":"bar",
            "yAxis":2
        },
        {
            "key":"Mujeres",
            "values":[
            {
                "x":2009,
                "y": 63
            },
            {
                "x":2010,
                "y": 75
            },
            {
                "x":2011,
                "y": 96
            },
            {
                "x":2012,
                "y": 33
            },
            {
                "x":2013,
                "y": 45
            },
            {
                "x":2014,
                "y": 47
            },
            {
                "x":2015,
                "y": 23
            },
            {
                "x":2016,
                "y": 36
            }
            ],
            "type":"bar",
            "yAxis":2
        },
        {
            "key":"Total",
            "values":[
            {
                "x":2009,
                "y": 123
            },
            {
                "x":2010,
                "y": 240
            },
            {
                "x":2011,
                "y": 189
            },
            {
                "x":2012,
                "y": 175
            },
            {
                "x":2013,
                "y": 110
            },
            {
                "x":2014,
                "y": 256
            },
            {
                "x":2015,
                "y": 210
            },
            {
                "x":2016,
                "y": 250
            }
            ],
            "type":"bar",
            "yAxis":2
        }
        ];

        vm.nodulesData = {
            series: ["Nodulo 1", "Nodulo 2", "Nodulo 3"],
            labels: ["2009","2010","2011", "2012", "2013", "2014", "2015", "2016"],
            data: [
            [6, 8, 8, 10, 14, 17,17,18],
            [5, 6, 8, 8, 10, 11, 12, 12],
            [6, 7, 8, 8, 9, 9, 10, 10]
            ]
        };

        vm.nodulesOptions = {
            legend: { display: true },
            datasetFill: true,
            lineTension: 0,
            pointRadius: 0
        };

        vm.nodulesColors = [
        {
            borderColor: "#1485C9",
            fill: false
        },
        {
            borderColor: "green",
            fill: false
        },
        {
            borderColor: "red",
            fill: false
        }];    
        
        vm.nodulesDataBar = {
            series: ["Hombres", "Mujeres", "Total"],
            labels: ["2009","2010","2011","2012","2013","2014","2015"],
            data: [
            [95, 76, 43, 80, 80, 150, 65, 71, 135],
            [70, 86, 98, 80, 90, 150, 65, 50, 115],
            [165, 162, 141, 160, 170, 300, 130, 121, 250]
            ]
        };

        vm.nodulesOptions = {
            legend: { display: true },
            datasetFill: true,
            lineTension: 0,
            pointRadius: 0
        };

        vm.nodulesColors = [
        {
            borderColor: "#1485C9",
            fill: false
        },
        {
            borderColor: "green",
            fill: false
        },
        {
            borderColor: "red",
            fill: false
        }]; 
    };


}
}());
