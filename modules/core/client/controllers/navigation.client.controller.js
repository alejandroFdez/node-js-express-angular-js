(function () {
  'use strict';

  angular
    .module('core')
    .controller('NavigationController', NavigationController);

  NavigationController.$inject = ['$scope', '$state', 'Authentication', 'menuService'];

  function NavigationController($scope, $state, Authentication, menuService) {
    var vm = this;

    vm.authentication = Authentication;
    vm.isCollapsed = false;
    vm.menu = menuService.getMenu('topbar');

    console.log("MENU")
    console.log(vm.menu)


    $scope.$on('$stateChangeSuccess', stateChangeSuccess);

    function stateChangeSuccess() {
      // Collapsing the menu after navigation
      vm.isCollapsed = false;
    }
  }
}());
