'use strict';

/**
 * Module dependencies
 */
var citasPolicy = require('../policies/citas.server.policy'),
  citas = require('../controllers/citas.server.controller');

module.exports = function(app) {
  // Citas Routes
  app.route('/api/citas').all(citasPolicy.isAllowed)
    .get(citas.list)
    .post(citas.create);

  app.route('/api/citas/:citaId').all(citasPolicy.isAllowed)
    .get(citas.read)
    .put(citas.update)
    .delete(citas.delete);
    
  /*app.route('/api/citas/paciente/:pacienteId').all(citasPolicy.isAllowed)
    .get(citas.list)*/

  // Finish by binding the Cita middleware
  app.param('citaId', citas.citaByID);
  
  // Finish by binding the Cita middleware
  //app.param('pacienteId', citas.pacienteByID);
};
