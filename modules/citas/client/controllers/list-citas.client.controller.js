(function () {
  'use strict';

  angular
    .module('citas')
    .controller('CitasListController', CitasListController);

  CitasListController.$inject = ['CitasService', 'CitasPacienteService', '$scope' ];

  function CitasListController( CitasService, CitasPacienteService, $scope ) {
      
    var vm = this;
    vm.paciente = $scope.paciente;
    //vm.citas = CitasService.query();
    
    if( vm.paciente == undefined )
    {
        vm.citas = CitasService.query();
    }
    else
    {
        vm.citas = CitasPacienteService.query({
            pacienteId: vm.paciente.resource.identifier[0].value
        });
    }
    
  }
}());
