(function (app) {
  'use strict';

  app.registerModule('citas', ['core']);// The core module is required for special route handling; see /core/client/config/core.client.routes
  app.registerModule('citas.services');
  app.registerModule('citas.routes', ['ui.router', 'core.routes', 'citas.services']);
}(ApplicationConfiguration));
