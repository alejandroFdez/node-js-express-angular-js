(function () {
  'use strict';

  angular
    .module('citas', ['chart.js'])
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(menuService) {
    // Set top bar menu items
    menuService.addMenuItem('topbar', {
      title: 'Citas',
      state: 'citas',
      iconClass: 'fa-bell',
      roles: ['user']
    });

    
  }
})();


