(function () {
  'use strict';

  angular
    .module('citas')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('citas', {
        url: '/citas',
        templateUrl: 'modules/citas/client/views/view-list-citas.client.view.html',
        controller: 'CitasListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Citas List'
        }
      })
      .state('citas.list', {
        url: '',
        templateUrl: 'modules/citas/client/views/view-list-citas.client.view.html',
        controller: 'CitasListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Citas List'
        }
      })
      .state('citas.create', {
        url: '/create',
        templateUrl: 'modules/citas/client/views/form-cita.client.view.html',
        controller: 'CitasController',
        controllerAs: 'vm',
        resolve: {
          citaResolve: newCita
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Citas Create'
        }
      })
      .state('citas.edit', {
        url: '/:citaId/edit',
        templateUrl: 'modules/citas/client/views/form-cita.client.view.html',
        controller: 'CitasController',
        controllerAs: 'vm',
        resolve: {
          citaResolve: getCita
        },
        data: {
          roles: ['user', 'admin'],
          pageTitle: 'Edit Cita {{ citaResolve.name }}'
        }
      })
      .state('citas.view', {
        url: '/:citaId',
        templateUrl: 'modules/citas/client/views/view-cita.client.view.html',
        controller: 'CitasController',
        controllerAs: 'vm',
        resolve: {
          citaResolve: getCita
        },
        data: {
          pageTitle: 'Cita {{ citaResolve.name }}'
        }
      });
  }

  getCita.$inject = ['$stateParams', 'CitasService'];

  function getCita($stateParams, CitasService) {
    return CitasService.get({
      citaId: $stateParams.citaId
    }).$promise;
  };
  

  newCita.$inject = ['CitasService'];

  function newCita(CitasService) {
    return new CitasService();
  };
}());
