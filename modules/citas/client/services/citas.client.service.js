// Citas service used to communicate Citas REST endpoints
(function () {
  'use strict';

  angular
    .module('citas')
    .factory('CitasService', CitasService);

  CitasService.$inject = ['$resource'];

  function CitasService($resource) {
    return $resource('api/citas/:citaId', {
      citaId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  };
  
}());
