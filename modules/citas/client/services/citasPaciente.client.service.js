// Citas service used to communicate Citas REST endpoints
(function () {
  'use strict';

  angular
    .module('citas')
    .factory('CitasPacienteService', CitasPacienteService);

  
  CitasPacienteService.$inject = ['$resource'];
  
  function CitasPacienteService($resource) { 
    return $resource('api/pacientes/:pacienteId/citas', {
      pacienteId: '@_id'
    });
  };
}());
