'use strict'; 

angular
.module( 'citas' )
.directive( 'citasList',  function()
{
    return {
        restrict: 'A',
        controller: 'CitasListController',
        controllerAs: 'vm',
        templateUrl: 'modules/citas/client/views/list-citas.client.view.html',
        scope: {
            paciente:'='
        }
    }
});
