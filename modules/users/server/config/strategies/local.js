'use strict';

/**
 * Module dependencies
 */
var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    ldap = require('ldapjs');
var client = ldap.createClient({
  url: 'ldap://ldap.forumsys.com:389'
});
  
  //User = require('mongoose').model('User');

module.exports = function () {
    // Use local strategy
    passport.use(new LocalStrategy({
      usernameField: 'username',
      passwordField: 'password'
    },
    function (username, password, done) {
        /*User.findOne({ 
          username: username.toLowerCase()
        }, function (err, user) {
          if (err) {
            return done(err);
          }
          if (!user || !user.authenticate(password)) {
            return done(null, false, {
              message: 'Invalid username or password'
            });
          } 

          return done(null, user);
        });*/
        
        var ldapres = null;

        /*client.bind('cn=read-only-admin,dc=example,dc=com', 'password', function(err){
            console.log(err);
        });*/
        
        var opts = {
            /*filter: '(uid='+ username +',dc=example,dc=com)',
            scope: 'sub'*/
        };

        client.search('uid='+ username +',dc=example,dc=com', opts, function (err, result) {
            console.log(err);
            result.on('searchEntry', function (entry) {
                ldapres = entry.raw;
            });
            
            result.on('searchReference', function(referral) {
                console.log('referral: ' + referral.uris.join());
            });
            result.on('error', function(err) {
                console.error('error: ' + err.message);
                return done(null, false, {
                    message: 'Invalid username'
                });
            });
            

            result.on('end', function (result) {
                console.log('status: ' + result.status);
                if (!ldapres) 
                { 
                    return done(null, false, {
                            message: 'Invalid username'
                        });
                }

                client.bind(ldapres.dn, password, function (err) {
                    if (err) 
                    {
                        return done(null, false, {
                            message: 'Invalid password'
                        });
                    }
                    var user = ldapres;
                    user =
                    {
                        "_id" : "5899da7e4ad967880c30893b",
                        "displayName" : "Alejandro Fernandez",
                        "provider" : "local",
                        "username" : "user",
                        "created" : "2017-02-07T14:32:30.835Z",
                        "roles" : [ 
                            "user"
                        ],
                        "profileImageURL" : "modules/users/client/img/profile/default.png",
                        "email" : "alejandrof.dez1993@gmail.com",
                        "lastName" : "Fernandez",
                        "firstName" : "Alejandro",
                        "__v" : 0
                    };
                    return done(null, user);
                    //res.send('You are logged')
                });

            });
        });
        /*if(username === 'user' && password === 'password')
        {
            var user =
            {
                "_id" : "5899da7e4ad967880c30893b",
                "displayName" : "Alejandro Fernandez",
                "provider" : "local",
                "username" : "user",
                "created" : "2017-02-07T14:32:30.835Z",
                "roles" : [ 
                    "user"
                ],
                "profileImageURL" : "modules/users/client/img/profile/default.png",
                "email" : "alejandrof.dez1993@gmail.com",
                "lastName" : "Fernandez",
                "firstName" : "Alejandro",
                "__v" : 0
            };
            return done(null, user);
        }
        else
        {
            return done(null, false, {
                message: 'Invalid username or password'
            });
        }*/

    }));
};
