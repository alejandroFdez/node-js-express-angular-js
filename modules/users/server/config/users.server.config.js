'use strict';

/**
 * Module dependencies
 */
var passport = require('passport'),
  //User = require('mongoose').model('User'),
  path = require('path'),
  config = require(path.resolve('./config/config'));

/**
 * Module init function
 */
module.exports = function (app/*, db*/) {
  // Serialize sessions
  /*passport.serializeUser(function (user, done) {
    done(null, user.id);
  });*/

  // Deserialize sessions
  /*passport.deserializeUser(function (id, done) {
    User.findOne({
      _id: id 
    }, '-salt -password', function (err, user) {
      done(err, user);
    });
    var user =
    {
        "_id" : "5899da7e4ad967880c30893b",
        "displayName" : "Alejandro Fernandez",
        "provider" : "local",
        "username" : "hierbis",
        "created" : "2017-02-07T14:32:30.835Z",
        "roles" : [ 
            "user"
        ],
        "profileImageURL" : "modules/users/client/img/profile/default.png",
        "email" : "alejandrof.dez1993@gmail.com",
        "lastName" : "Fernandez",
        "firstName" : "Alejandro",
        "__v" : 0
    };
    done(null, user); 
  });*/
    
  passport.serializeUser(function(user, done) {
    done(null, user);
  });

  passport.deserializeUser(function(user, done) {
    done(null, user);
  });

  // Initialize strategies
  config.utils.getGlobbedPaths(path.join(__dirname, './strategies/**/*.js')).forEach(function (strategy) {
    require(path.resolve(strategy))(config);
  });

  // Add passport's middleware
  app.use(passport.initialize());
  app.use(passport.session());
};
